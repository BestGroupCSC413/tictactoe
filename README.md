# README #

### Multiplayer Tic Tac Toe GAME ###

* Tic Tac Toe game which allows two users to play between each other
* Version 1.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Compile this game on your computer and your oponent computer, 
* or compile it on your computer and use two emulators in order to test the game. 
* Or upload this game into two android phones
* From main menu players can choose if they would like to play a game or view a leader board.
* When player is choses to play a game, he/she will be taken to the activity where they 
* can choose to play one of the current games, or invite a new person to the game.
* After the game is over, player will be taken to  leader board activity, where he/ she can oserve their progrees in the game.

### Contribution guidelines ###

* This project was created as a final project for SFSU CSC413 class.

### Who do I talk to? ###

* Elina Suslova: bitbucket ID: elinas11
* Javier Quiroz: bitbucket ID: JavQuiMor
* Sean Sutherland: bitbucket ID: srsutherland
* Anthony Branda: bitbucket ID: abranda
* Lily Linh Lan: bitbucket ID: lilylinhlan