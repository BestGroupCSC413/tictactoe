package com.example.mac.tictactoe;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class FindGameActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener{
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private final String TAG = "HomeActivity";
    private FirebaseUser mUser;
    private GoogleApiClient mGoogleApiClient;
    public static final String PLAYERS_CHILD = "players";
    private static DatabaseReference sFirebaseDatabaseReference =
            FirebaseDatabase.getInstance().getReference();
    private RecyclerView currentGamesRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private FirebaseRecyclerAdapter currentGamesFirebaseAdapter;
    private RecyclerView playersRecyclerView;
    private FirebaseRecyclerAdapter playersFirebaseAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_game);
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Initialize RecyclerView.
        playersRecyclerView = (RecyclerView) findViewById(R.id.playersRecyclerView);
        mLinearLayoutManager = new LinearLayoutManager(this);
        // Causes unwanted flickery effect
        playersRecyclerView.setItemAnimator(null);
        mLinearLayoutManager.setReverseLayout(false);
        mLinearLayoutManager.setStackFromEnd(true);
        playersRecyclerView.setLayoutManager(mLinearLayoutManager);

        playersFirebaseAdapter = UserUtil.getFirebaseAdapter(this,
                mLinearLayoutManager,
                currentGamesRecyclerView);

        playersRecyclerView.setAdapter(playersFirebaseAdapter);

        // Initialize RecyclerView.
        currentGamesRecyclerView = (RecyclerView) findViewById(R.id.currentGamesRecyclerView);
        mLinearLayoutManager = new LinearLayoutManager(this);
        // Causes unwanted flickery effect
        currentGamesRecyclerView.setItemAnimator(null);
        mLinearLayoutManager.setReverseLayout(false);
        mLinearLayoutManager.setStackFromEnd(true);
        currentGamesRecyclerView.setLayoutManager(mLinearLayoutManager);

        currentGamesFirebaseAdapter = GameRef.getFirebaseAdapter(this,  /* MessageLoadListener */
                mLinearLayoutManager,
                currentGamesRecyclerView, mUser.getUid());

        currentGamesRecyclerView.setAdapter(currentGamesFirebaseAdapter);
    }

    protected DatabaseReference createGame(String player0, String player1) {
        DatabaseReference gameRef = sFirebaseDatabaseReference.child(CHILD.GAMES).push();
        String gameid = gameRef.getKey();
        Game newGame = new Game(gameid, player0, player1);
        sFirebaseDatabaseReference.child(CHILD.GAMES).child(gameid).setValue(newGame);
        sFirebaseDatabaseReference.child(CHILD.GAMES_BY_USER).child(player0).child(gameid).setValue(
                new GameRef(gameid, player1)
        ); //delete this once game is finished
        sFirebaseDatabaseReference.child(CHILD.GAMES_BY_USER).child(player1).child(gameid).setValue(
                new GameRef(gameid, player0)
        ); //delete this once game is finished
        return gameRef;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }

    public void onClickPlayer(String otherPlayer) {
        createGame(mUser.getUid(), otherPlayer);
    }

    public void onClickGame(String gameid) {
        launchGame(gameid);
    }

    public void launchGame(String gameid){
        Intent gameIntent = new Intent(FindGameActivity.this, GameActivity.class)
                .putExtra("gameid", gameid)
                .putExtra("user", mUser.getUid());
        startActivity(gameIntent);
    }
}
