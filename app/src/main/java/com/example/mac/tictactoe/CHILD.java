package com.example.mac.tictactoe;

/**
 * Created by Sean Sutherland on 12/6/2017.
 * Static class containing "child" keys for all the Firebase nodes
 */

@SuppressWarnings("WeakerAccess")
public class CHILD {
    public static final String USERS = "users";
    public static final String GAMES_BY_USER = "gamesbyuser";
    public static final String GAMES = "games";
    public static final String WINS = "wins";
}
