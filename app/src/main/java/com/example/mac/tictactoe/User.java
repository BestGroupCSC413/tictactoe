package com.example.mac.tictactoe;

import android.graphics.BitmapFactory;
import android.widget.ImageView;

/**
 * Created by phuonglinh on 12/3/17.
 */

public class User {
    public static final int TOGGLE_WINS = 1;
    public static final int TOGGLE_LOOSES = 2;
    public static final int TOGGLE_TIES = 3;

    private String username;
    private String uid;
    private byte[] avatar;
    private int wins;
    private int looses;
    private int ties;
    public static final byte[] defaultAvatar = null;

    public User(String username, byte[] avatar, int wins, int looses, int ties) {
        this.username = username;
        this.avatar = avatar;
        this.wins = wins;
        this.looses = looses;
        this.ties = ties;
        this.uid = "no_uid";

    }

    public User(String username, String uid, byte[] avatar, int wins, int looses, int ties) {
        this.username = username;
        this.avatar = avatar;
        this.wins = wins;
        this.looses = looses;
        this.ties = ties;
        this.uid = uid;
    }

    public User(String username, byte[] avatar) {
        this(username, avatar, 0, 0, 0);
    }

    public User(String username, String uid, byte[] avatar) {
        this(username, uid,  avatar, 0, 0, 0);
    }

    public User(){
        this("null", "no_id", null, 0,0,0);
    }

    public String getUsername() {
        return username;
    }

    public byte[] getAvatar() {
        return avatar;
    }

    public int getWins() {
        return wins;
    }

    public int getLooses() {
        return looses;
    }

    public int getTies() {
        return ties;
    }

    public void updateScore(int action) {
        if (action == TOGGLE_WINS) {
            wins++;
        } else if (action == TOGGLE_LOOSES) {
            looses++;
        } else {
            ties++;
        }
    }

    public void setImage(ImageView image ) {
        image.setImageBitmap(BitmapFactory.decodeByteArray(avatar, 0, avatar.length));
    }

    public String getUID() {
        return uid;
    }

    public void copy(User other) {
        this.username = other.username;
        this.avatar = other.avatar;
        this.uid = other.uid;
        this.wins = other.wins;
        this.looses = other.looses;
        this.ties = other.ties;
    }

}
