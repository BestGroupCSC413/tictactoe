package com.example.mac.tictactoe;

import java.util.Random;

/**
 * Created by Sean Sutherland on 12/5/2017.
 */

public class Game {
    private final String uid;
    private final String player0;
    private final String player1;
    private int turn;
    private boolean finished;
    private int buttonPressed;
    //todo: add board state (gamelogic branch)
    public Game(){
        this("no_id","null", "null");
        this.turn = 0;
        this.finished = false;

    }

    public Game(String uid, String player0, String player1) {
        this.uid = uid;
        this.player0 = player0;
        this.player1 = player1;
       // this.turn = (int) (Math.random() * 2); // Random player starts, remove if you want
        this.finished = false;
        this.turn = 0;
        this.buttonPressed = 0;
        //initialise board state
    }

    public String getUID() {
        return uid;
    }

    public String getPlayer(int n) {
        if (n==0) return player0;
        else return player1;
    }
    public int getButtonPressed(){return buttonPressed;}
    public String getCurrentPlayer() {
        return getPlayer(turn);
    }

    public String getPlayer0() {
        return player0;
    }

    public String getPlayer1() {
        return player1;
    }

    public int getTurn() {
        return turn;
    }

    public boolean isFinished() {return this.finished;}

    private int advanceTurn() {
       // turn = turn == 0 ? 1 : 0;
        if(getTurn()==0){
            turn = 1;
        }
        else turn = 0;
        return turn;
    }

    public void makeMove( /*adjust method signature as needed*/ ) { //todo: gamelogic
        /* change board state state */
        advanceTurn();
        //save to firebase after this is called
    }
}
