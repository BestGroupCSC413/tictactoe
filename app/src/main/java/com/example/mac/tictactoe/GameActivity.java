package com.example.mac.tictactoe;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ActionMenuView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Transaction.Handler;
import com.google.firebase.database.ValueEventListener;
import java.util.Map;



/**
 * Created by elinasuslova on 11/16/17.
 */

public class GameActivity extends AppCompatActivity  {

  public final String TAG = "GameActivity";
  public static final char firstPlayer = 'X';
  public static final char secondPlayer = 'O';
  private char currentPlayer; //= 'O';
  private boolean gameIsOver = false;
  private DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
  private FirebaseAuth mAuth;
  private FirebaseUser mUser;


  // in button naming r corresponds to row and c to column of the button
  private Button button1r1c1, button2r1c2, button3r1c3, button4r2c1, button5r2c2, button6r2c3;
  private Button button7r3c1, button8r3c2, button9r3c3;
  boolean b1Xclicked, b2Xclicked, b3Xclicked, b4Xclicked,b5Xclicked,b6Xclicked,
      b7Xclicked,b8Xclicked,b9Xclicked = false;
  boolean b1Oclicked, b2Oclicked, b3Oclicked, b4Oclicked,b5Oclicked,b6Oclicked,
      b7Oclicked,b8Oclicked,b9Oclicked = false;
  private boolean myTurn = true;
  int count = 0;
  private TextView gameResultToDisplay;
  String player1;
  String player2;
  String gameId;
  String gameResult;
  int buttonPressed;
  int player = 1;
  String currentGamePlayer;




  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Bundle extras = getIntent().getExtras();
    player2 = extras.getString("user");
    gameId = extras.getString("gameid");
    mAuth = FirebaseAuth.getInstance();
    mUser = mAuth.getCurrentUser();
    player1 = mUser.getUid();

    setContentView(R.layout.activity_game);
    button1r1c1 = (Button) findViewById(R.id.row1column1);
    button2r1c2 = (Button) findViewById(R.id.row1column2);
    button3r1c3 = (Button) findViewById(R.id.row1column3);
    button4r2c1 = (Button) findViewById(R.id.row2column1);
    button5r2c2 = (Button) findViewById(R.id.row2column2);
    button6r2c3 = (Button) findViewById(R.id.row2column3);
    button7r3c1 = (Button) findViewById(R.id.row3column1);
    button8r3c2 = (Button) findViewById(R.id.row3column2);
    button9r3c3 = (Button) findViewById(R.id.row3column3);

    gameResultToDisplay = (TextView) findViewById(R.id.gameResultDisplay);


    mRootRef.child("games").child(gameId).child("turn").setValue(0);
    mRootRef.child("games").child(gameId).child("buttonPressed").setValue(0);
    mRootRef.child("games").child(gameId).child("finished").setValue(false);
    mRootRef.child("games").child(gameId).child("counter").setValue(0);

   // mRootRef.child("games").child(gameId).child("currentPlayer").setValue(player1);

    mRootRef.child("games").child(gameId).addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {
        buttonPressed = Integer.parseInt(dataSnapshot.child("buttonPressed").getValue().toString());
        currentGamePlayer = dataSnapshot.child("currentPlayer").getValue().toString();}

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }

    }
    );



    //mRootRef.child("games").child(gameId).addValueEventListener(new ValueEventListener() {
    mRootRef.child("games").child(gameId).child("turn").addValueEventListener(new ValueEventListener() {
      @Override
      public void onDataChange(DataSnapshot dataSnapshot) {

        //Game game  = dataSnapshot.getValue(Game.class);
        //int turn = game.getTurn();
        //String myPlayer = game.getPlayer1();
        //updateImage();
        //player1 = myPlayer;

        //buttonPressed = game.getButtonPressed();
        //player = Integer.parseInt(dataSnapshot.child("games").child(gameId).child("turn").getValue().toString());

        //player = Integer.parseInt(dataSnapshot.child("turn").getValue().toString());
        player = Integer.parseInt(dataSnapshot.getValue().toString());
        //buttonPressed = Integer.parseInt(dataSnapshot.child("buttonPressed").getValue().toString());
        //currentGamePlayer = dataSnapshot.child("currentPlayer").getValue().toString();

        //buttonPressed = Integer.parseInt(dataSnapshot.child("games").child(gameId).child("buttonPressed").getValue().toString());
        //buttonPressed = mRootRef.child("games").child(gameId).child("buttonPressed");
         // drawShape(buttonPressed, player+1);

        //updateImage(buttonPressed,2);
        drawShape(buttonPressed, player+1);
        //drawShape(buttonPressed, 2);

        //mRootRef.child("games").child(gameId).child("turn").setValue(turn);
        //Log.v("E_VALUE","TURN :"+dataSnapshot.getValue());
       if (checkForWinner()) {
          //A winner!! send info to database
          mRootRef.child("games").child(gameId).child("finished").setValue(true);
        }
        if (checkForTie()) {
          //Tie, send info to DB and end game
          mRootRef.child("games").child(gameId).child("finished").setValue(true);
        }



         if(player == 1){
          currentPlayer  = firstPlayer;
          //mRootRef.child("games").child(gameId).child("turn").setValue(0);
         // myTurn = true;
        }
        else{
          //mRootRef.child("games").child(gameId).child("turn").setValue(1);
          currentPlayer = secondPlayer;


          //myTurn = false;


        }
       /*
        if(currentGamePlayer == player1){
          myTurn = true;

        }
        else if(currentGamePlayer == player2){
          myTurn = false;
        }
        */


      }



      @Override
      public void onCancelled(DatabaseError databaseError) {

      }
    });


    button1r1c1.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {

        // X player turn
        if (currentPlayer == firstPlayer) {
         b1Xclicked = true;
          //mRootRef.child("games").child(gameId).child("currentPlayer").setValue(player2);
          //imageCircleR1C1.setVisibility(View.VISIBLE);
          updateImage(1, 1);

        }
        // O player turn
        else if (currentPlayer == secondPlayer) {
         b1Oclicked = true;
          updateImage(1, 2);

        }

        // if button is clicked
        if (checkForWinner()) {
          //A winner!! send info to database
          mRootRef.child("games").child(gameId).child("finished").setValue(true);

        }
        if (checkForTie()) {
          //Tie, send info to DB and end game
          mRootRef.child("games").child(gameId).child("finished").setValue(true);

        }


      }

    });

    button2r1c2.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {

        if (currentPlayer == firstPlayer) {
          updateImage(2, 1);
         b2Xclicked = true;

        } else if (currentPlayer == secondPlayer) {
         b2Oclicked = true;
          //imageXR1C2.setVisibility(View.VISIBLE);
          updateImage(2, 2);

        }
        //if button is clicked
        if (checkForWinner()) {
          //A winner!! send info to database
          mRootRef.child("games").child(gameId).child("finished").setValue(true);
        }
        if (checkForTie()) {
          //Tie, send info to DB and end game
          mRootRef.child("games").child(gameId).child("finished").setValue(true);
        }




      }
    });

    button3r1c3.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {
        if (currentPlayer == firstPlayer) {
          b3Xclicked = true;
          updateImage(3, 1);

        } else if (currentPlayer == secondPlayer) {
          b3Oclicked = true;
          updateImage(3, 2);

        }
        //if button is clicked
        if (checkForWinner()) {
          //A winner!! send info to database
          mRootRef.child("games").child(gameId).child("finished").setValue(true);
        }
        if (checkForTie()) {
          //Tie, send info to DB and end game
          mRootRef.child("games").child(gameId).child("finished").setValue(true);
        }


      }
    });

    button4r2c1.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {
        if (currentPlayer == firstPlayer) {
          b4Xclicked = true;
          updateImage(4, 1);

        } else if (currentPlayer == secondPlayer) {
         b4Oclicked = true;
          updateImage(4, 2);

        }
        //if button is clicked
        if (checkForWinner()) {
          //A winner!! send info to database
          mRootRef.child("games").child(gameId).child("finished").setValue(true);
        }
        if (checkForTie()) {
          //Tie, send info to DB and end game
          mRootRef.child("games").child(gameId).child("finished").setValue(true);
        }


      }
    });

    button5r2c2.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {
        if (currentPlayer == firstPlayer) {
          b5Xclicked = true;
          updateImage(5, 1);

        } else if (currentPlayer == secondPlayer) {
          b5Oclicked = true;
          updateImage(5, 2);

        }
        //if button is clicked
        if (checkForWinner()) {
          //A winner!! send info to database
          mRootRef.child("games").child(gameId).child("finished").setValue(true);
        }
        if (checkForTie()) {
          //Tie, send info to DB and end game
          mRootRef.child("games").child(gameId).child("finished").setValue(true);
        }


      }
    });

    button6r2c3.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {
        if (currentPlayer == firstPlayer) {
          b6Xclicked = true;
          updateImage(6, 1);
        } else if (currentPlayer == secondPlayer) {
          b6Oclicked = true;
          updateImage(6, 2);
        }
        //if button is clicked
        if (checkForWinner()) {
          //A winner!! send info to database
          mRootRef.child("games").child(gameId).child("finished").setValue(true);
        }
        if (checkForTie()) {
          //Tie, send info to DB and end game
          mRootRef.child("games").child(gameId).child("finished").setValue(true);
        }




      }
    });

    button7r3c1.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {
        if (currentPlayer == firstPlayer) {
          b7Xclicked = true;
          updateImage(7, 1);
        } else if (currentPlayer == secondPlayer) {
         b7Oclicked = true;
          updateImage(7, 2);
        }
        //if button is clicked
        if (checkForWinner()) {
          //A winner!! send info to database
          mRootRef.child("games").child(gameId).child("finished").setValue(true);
        }
        if (checkForTie()) {
          //Tie, send info to DB and end game
          mRootRef.child("games").child(gameId).child("finished").setValue(true);
        }



      }
    });

    button8r3c2.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {
        if (currentPlayer == firstPlayer) {
          b8Xclicked = true;
          updateImage(8, 1);
        } else if (currentPlayer == secondPlayer) {
          b8Oclicked = true;
          updateImage(8, 2);
        }
        //if button is clicked
        if (checkForWinner()) {
          //A winner!! send info to database
          mRootRef.child("games").child(gameId).child("finished").setValue(true);
        }
        if (checkForTie()) {
          //Tie, send info to DB and end game
          mRootRef.child("games").child(gameId).child("finished").setValue(true);
        }



      }
    });

    button9r3c3.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {
        if (currentPlayer == firstPlayer) {
          b9Xclicked = true;
          updateImage(9, 1);
        } else if (currentPlayer == secondPlayer) {
          b9Oclicked = true;

          updateImage(9, 2);
        }
        //if button is clicked
        if (checkForWinner()) {

          //A winner!! send info to database
          mRootRef.child("games").child(gameId).child("finished").setValue(true);
        }
        if (checkForTie()) {
          //Tie, send info to DB and end game
          mRootRef.child("games").child(gameId).child("finished").setValue(true);
        }




      }
    });

  }




  private boolean checkColumnsForWinX() {
      if (((b1Xclicked && b4Xclicked && b7Xclicked)||(b2Xclicked && b5Xclicked && b8Xclicked ) ||
          (b3Xclicked && b6Xclicked && b9Xclicked)) == true){
        return true;
      }
    return false;
  }
  private boolean checkRowsForWinX(){
    if(((b1Xclicked && b2Xclicked && b3Xclicked ) || (b4Xclicked && b5Xclicked && b6Xclicked) ||
        (b7Xclicked && b8Xclicked && b9Xclicked)) == true){
      return true;
    }
    return false;
  }
  private boolean checkDiagonalForWinX(){
    if(((b1Xclicked && b5Xclicked && b9Xclicked) || (b3Xclicked && b5Xclicked && b7Xclicked)) == true){
      return true;
    }
    return false;

  }

  public boolean checkForWinnerX() {
    return(checkColumnsForWinX() || checkRowsForWinX() || checkDiagonalForWinX());
  }

  private boolean checkRowsForWinO(){
    if(((b1Oclicked && b2Oclicked && b3Oclicked ) || (b4Oclicked && b5Oclicked && b6Oclicked) ||
        (b7Oclicked && b8Oclicked && b9Oclicked)) == true){
      return true;
    }
    return false;
  }
  private boolean checkDiagonalForWinO(){
    if(((b1Oclicked && b5Oclicked && b9Oclicked) || (b3Oclicked && b5Oclicked && b7Oclicked)) == true){
      return true;
    }
    return false;

  }
  private boolean checkColumnsForWinO() {
    if (((b1Oclicked && b4Oclicked && b7Oclicked) || (b2Oclicked && b5Oclicked && b8Oclicked) ||
        (b3Oclicked && b6Oclicked && b9Oclicked)) == true){
      return true;
    }
    return false;
  }


  public boolean checkForWinnerO() {
    return(checkColumnsForWinO() || checkRowsForWinO() || checkDiagonalForWinO());
  }

  public boolean checkForWinner(){
    if(checkForWinnerO()){
      gameResultToDisplay.setText("Player O Won");
      gameIsOver = true;
      gameResult = currentGamePlayer;
      launchLeaderBoard();


    }
    else if(checkForWinnerX()){
      gameResultToDisplay.setText("Player X Won");
      gameIsOver = true;
      gameResult = currentGamePlayer;
      launchLeaderBoard();
    }
    return ( checkForWinnerO() || checkForWinnerX());

  }

  public boolean checkForTie(){
    if ((count == 9) && (!checkForWinner())){
      gameResultToDisplay.setText("Tie Game");
      gameIsOver = true;
      gameResult = "tie";
      launchLeaderBoard();


      return true;
    }
      return false;
  }
  public void onResume()
  {
    super.onResume();
    b1Xclicked = b2Xclicked = b3Xclicked = b4Xclicked = b5Xclicked = b6Xclicked = b7Xclicked = b8Xclicked = b9Xclicked = false;
    b1Oclicked = b2Oclicked = b3Oclicked = b4Oclicked = b5Oclicked = b6Oclicked = b7Oclicked = b8Oclicked = b9Oclicked = false;
    count = 0;
    mRootRef.child("games").child(gameId).child("turn").setValue(0);
    mRootRef.child("games").child(gameId).child("buttonPressed").setValue(0);
    mRootRef.child("games").child(gameId).child("finished").setValue(false);
    mRootRef.child("games").child(gameId).child("counter").setValue(0);


  }
  public void updateImage(int button, int player) {

    if (!myTurn || gameIsOver) {
      getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
              WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }
    else if (myTurn && !gameIsOver) {

      getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);





      if (currentPlayer == firstPlayer) {

        mRootRef.child("games").child(gameId).child("buttonPressed").setValue(button);
        mRootRef.child("games").child(gameId).child("counter").setValue(count);
        mRootRef.child("games").child(gameId).child("currentPlayer").setValue(player1);
        mRootRef.child("games").child(gameId).child("turn").setValue(0);


      } else {

        mRootRef.child("games").child(gameId).child("buttonPressed").setValue(button);
        mRootRef.child("games").child(gameId).child("counter").setValue(count);
        mRootRef.child("games").child(gameId).child("currentPlayer").setValue(player2);
        mRootRef.child("games").child(gameId).child("turn").setValue(1);

      }

        //currentPlayer = firstPlayer;



      //myTurn = false;
      }
    //drawShape(button, player);
    //getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
      //      WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

  }

    public void drawShape(int button, int player) {

      int backimage;

      if (player == 1)
        backimage = R.drawable.x2tictactoe;

      else
        backimage = R.drawable.circle2tictactoe;


      switch (button) {
        case 1:
          button1r1c1.setBackgroundResource(backimage);
          count++;
          button1r1c1.setClickable(false);
          if(player == 1){
            b1Xclicked = true;
          }
          else{
            b1Oclicked = true;
          }
          break;
        case 2:
          button2r1c2.setBackgroundResource(backimage);
          count++;
          button2r1c2.setClickable(false);
          if(player == 1){
            b2Xclicked = true;
          }
          else{
            b2Oclicked = true;
          }
          break;
        case 3:
          button3r1c3.setBackgroundResource(backimage);
          count++;
          button3r1c3.setClickable(false);
          if(player == 1){
            b3Xclicked = true;
          }
          else{
            b3Oclicked = true;
          }
          break;
        case 4:
          button4r2c1.setBackgroundResource(backimage);
          count++;
          button4r2c1.setClickable(false);
          if(player == 1){
            b4Xclicked = true;
          }
          else{
            b4Oclicked = true;
          }
          break;
        case 5:
          button5r2c2.setBackgroundResource(backimage);
          count++;
          button5r2c2.setClickable(false);
          if(player == 1){
            b5Xclicked = true;
          }
          else{
            b5Oclicked = true;
          }
          break;
        case 6:
          button6r2c3.setBackgroundResource(backimage);
          count++;
          button6r2c3.setClickable(false);
          if(player == 1){
            b6Xclicked = true;
          }
          else{
            b6Oclicked = true;
          }
          break;
        case 7:
          button7r3c1.setBackgroundResource(backimage);
          count++;
          button7r3c1.setClickable(false);
          if(player == 1){
            b7Xclicked = true;
          }
          else{
            b7Oclicked = true;
          }
          break;
        case 8:
          button8r3c2.setBackgroundResource(backimage);
          count++;
          button8r3c2.setClickable(false);
          if(player == 1){
            b8Xclicked = true;
          }
          else{
            b8Oclicked = true;
          }
          break;
        case 9:
          button9r3c3.setBackgroundResource(backimage);
          count++;
          button9r3c3.setClickable(false);
          if(player == 1){
            b9Xclicked = true;
          }
          else{
            b9Oclicked = true;
          }
      }
    }




  public void launchLeaderBoard() {

    Intent activityIntent = new Intent(GameActivity.this, LeaderBoardActivity.class)
           .putExtra("gameid", gameId)
           .putExtra("player1", player1)
           .putExtra("player2", player2)
           .putExtra("winner", gameResult);

       startActivity(activityIntent);
     }


   }




