package com.example.mac.tictactoe;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

public class LeaderBoardActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    public static boolean canIncrease = true;

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private final String TAG = "leaderboard";
    private GoogleApiClient mGoogleApiClient;
    private DrawerLayout mDrawerLayout;
    private Toolbar mToolBar;
    private TextView[] mRank;
    private TextView[] mUsername;
    private TextView[] mWins;
    private TextView[] mTies;
    private int mLeaderBoardOffset = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leader_board);
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                if (mUser != null) {
                    // User is signed in
                    //getSupportActionBar().setTitle(mUser.getEmail());
                } else {
                    // User is signed out
                    Log.d(TAG, "not logged in");
                    Intent signInIntent = new Intent(getApplicationContext(), SignInActivity.class);
                    startActivity(signInIntent);
                    finish();
                }
            }
        };

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerRoot);
        mToolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolBar);

        NavigationView mNavigationView = (NavigationView) findViewById(R.id.nav_view);

        mRank = new TextView[10];
        mRank[0] = (TextView)findViewById(R.id.rank1);
        mRank[1] = (TextView)findViewById(R.id.rank2);
        mRank[2] = (TextView)findViewById(R.id.rank3);
        mRank[3] = (TextView)findViewById(R.id.rank4);
        mRank[4] = (TextView)findViewById(R.id.rank5);
        mRank[5] = (TextView)findViewById(R.id.rank6);
        mRank[6] = (TextView)findViewById(R.id.rank7);
        mRank[7] = (TextView)findViewById(R.id.rank8);
        mRank[8] = (TextView)findViewById(R.id.rank9);
        mRank[9] = (TextView)findViewById(R.id.rank10);

        mUsername = new TextView[10];
        mUsername[0] = (TextView)findViewById(R.id.user1);
        mUsername[1] = (TextView)findViewById(R.id.user2);
        mUsername[2] = (TextView)findViewById(R.id.user3);
        mUsername[3] = (TextView)findViewById(R.id.user4);
        mUsername[4] = (TextView)findViewById(R.id.user5);
        mUsername[5] = (TextView)findViewById(R.id.user6);
        mUsername[6] = (TextView)findViewById(R.id.user7);
        mUsername[7] = (TextView)findViewById(R.id.user8);
        mUsername[8] = (TextView)findViewById(R.id.user9);
        mUsername[9] = (TextView)findViewById(R.id.user10);

        mWins = new TextView[10];
        mWins[0] = (TextView)findViewById(R.id.wins1);
        mWins[1] = (TextView)findViewById(R.id.wins2);
        mWins[2] = (TextView)findViewById(R.id.wins3);
        mWins[3] = (TextView)findViewById(R.id.wins4);
        mWins[4] = (TextView)findViewById(R.id.wins5);
        mWins[5] = (TextView)findViewById(R.id.wins6);
        mWins[6] = (TextView)findViewById(R.id.wins7);
        mWins[7] = (TextView)findViewById(R.id.wins8);
        mWins[8] = (TextView)findViewById(R.id.wins9);
        mWins[9] = (TextView)findViewById(R.id.wins10);

        mTies = new TextView[10];
        mTies[0] = (TextView)findViewById(R.id.ties1);
        mTies[1] = (TextView)findViewById(R.id.ties2);
        mTies[2] = (TextView)findViewById(R.id.ties3);
        mTies[3] = (TextView)findViewById(R.id.ties4);
        mTies[4] = (TextView)findViewById(R.id.ties5);
        mTies[5] = (TextView)findViewById(R.id.ties6);
        mTies[6] = (TextView)findViewById(R.id.ties7);
        mTies[7] = (TextView)findViewById(R.id.ties8);
        mTies[8] = (TextView)findViewById(R.id.ties9);
        mTies[9] = (TextView)findViewById(R.id.ties10);

        Button down10 = (Button)findViewById(R.id.down10);
        Button up10 = (Button)findViewById(R.id.up10);
        Button menu = (Button)findViewById(R.id.menu);

        UpdateLeaderBoard();

        down10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mLeaderBoardOffset > 0) {
                    mLeaderBoardOffset--;
                    UpdateLeaderBoard();
                    canIncrease = true;
                }
            }
        });

        up10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DBAdapter.getUsersSize(LeaderBoardActivity.this, mLeaderBoardOffset);
                if(canIncrease) {
                    mLeaderBoardOffset++;
                    UpdateLeaderBoard();
                }
            }
        });

        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(LeaderBoardActivity.this, HomeActivity.class));
            }
        });

        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                String txt;
                Intent i;
                // Fill this out just like your normal menus
                switch (menuItem.getItemId()) {
                    case R.id.leaderboard:
                        txt = "Leader Board";
                        break;
                    default:
                        txt = "Item Was Clicked";
                }
                Toast.makeText(getApplicationContext(), txt, Toast.LENGTH_LONG).show();
                mDrawerLayout.closeDrawers();
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.logout:
                mAuth.signOut();
                Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                startActivity(new Intent(this, SignInActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        Toast.makeText(this, "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }

    private void UpdateLeaderBoard(){
        List<User> topten = new ArrayList<>();
        DBAdapter.getTopTenUsers(this, mLeaderBoardOffset);
    }

    public void setLeaderText(List<User> topten) {
        for(int i = 0+(10*mLeaderBoardOffset); i < 10+(10*mLeaderBoardOffset); i++) {
            if (i < topten.size()) {
                mRank[i-(10*mLeaderBoardOffset)].setText("" + (i + 1));
                mUsername[i-(10*mLeaderBoardOffset)].setText(topten.get(i).getUsername());
                mWins[i-(10*mLeaderBoardOffset)].setText("" + topten.get(i).getWins());
                mTies[i-(10*mLeaderBoardOffset)].setText("" + topten.get(i).getTies());
            } else {
                mRank[i-(10*mLeaderBoardOffset)].setText("" + (i + 1));
                mUsername[i-(10*mLeaderBoardOffset)].setText("");
                mWins[i-(10*mLeaderBoardOffset)].setText("");
                mTies[i-(10*mLeaderBoardOffset)].setText("");
            }
        }
    }
};
