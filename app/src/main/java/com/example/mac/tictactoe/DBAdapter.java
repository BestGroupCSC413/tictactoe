package com.example.mac.tictactoe;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Adapter;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class DBAdapter {
    private static DatabaseReference sFirebaseDatabaseReference =
            FirebaseDatabase.getInstance().getReference();

    public static void getUser(String userid, final User user, final RecyclerView.Adapter adapter) {
        sFirebaseDatabaseReference.child(CHILD.USERS).child(userid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User tmpUser = dataSnapshot.getValue(User.class);
                user.copy(tmpUser);
                if (adapter != null) {
                    adapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static void updateUser(String uid, final int action) {
        sFirebaseDatabaseReference.child(CHILD.USERS).child(uid).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                user.updateScore(action);
                sFirebaseDatabaseReference.child(CHILD.USERS).child(user.getUID()).setValue(user);
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public static void getTopTenUsers(final LeaderBoardActivity leaderBoardActivity, int offset) {
        sFirebaseDatabaseReference.child(CHILD.USERS).orderByChild(CHILD.WINS).limitToLast(10+(10*offset)).addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<User> results = new ArrayList<>();
                        for(DataSnapshot children: dataSnapshot.getChildren()){
                            User user = children.getValue(User.class);
                            results.add(user);
                        }
                        Collections.reverse(results);
                        leaderBoardActivity.setLeaderText(results);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }
        );
    }

    public static void getUsersSize(final LeaderBoardActivity leaderBoardActivity, final int offset) {
        sFirebaseDatabaseReference.child(CHILD.USERS).addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<User> results = new ArrayList<>();
                        for(DataSnapshot children: dataSnapshot.getChildren()){
                            User user = children.getValue(User.class);
                            results.add(user);
                        }
                        if(results.size() > 20+(10*offset))
                            leaderBoardActivity.canIncrease = true;
                        else
                            leaderBoardActivity.canIncrease = false;
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }
        );
    }



    /////////////////////////////////////////////////////////////////////
    //	Constants & Data
    /////////////////////////////////////////////////////////////////////
    // Users table
//    public static final String ROWID = "_id";
//    public static final String USERNAME = "username";
//    public static final String AVATAR = "avatar";
//    public static final String WINS = "wins";
//    public static final String LOOSES = "looses";
//    public static final String TIES = "ties";
//
//    public static final String[] ALL_USER_COLUMNS = new String[] {USERNAME, AVATAR, WINS, LOOSES, TIES};
//
//    // DB info: it's name, and the table we are using (just one).
//    public static final String DATABASE_NAME = "tictactoe";
//    public static final String USER_TABLE = "users";
//
//    // Track DB version if a new version of your app changes the format.
//    public static final int DATABASE_VERSION = 1;
//
//    private static final String CREATE_USER_SQL =
//            "create table " + USER_TABLE
//                    + " (" + ROWID + " integer primary key autoincrement, "
//                    + USERNAME + " text not null, "
//                    + AVATAR + "blob, "
//                    + WINS + " integer not null, "
//                    + LOOSES + " integer not null, "
//                    + TIES + " integer not null);";
//
//    // Context of application who uses us.
//    private final Context context;
//    private DatabaseHelper myDBHelper;
//    private SQLiteDatabase db;
//
//    /////////////////////////////////////////////////////////////////////
//    //	Public methods:
//    /////////////////////////////////////////////////////////////////////
//
//    public DBAdapter(Context ctx) {
//        this.context = ctx;
//        myDBHelper = new DatabaseHelper(context);
//    }
//
//    // Open the database connection.
//    public DBAdapter open() {
//        db = myDBHelper.getWritableDatabase();
//        return this;
//    }
//
//    // Close the database connection.
//    public void close() {
//        myDBHelper.close();
//    }
//
//    /**
//     * Adds a new user into the database
//     * @param user new user to add
//     * @return internal user id
//     */
//    public long addUser(User user) {
//        // Create row's data:
//        ContentValues initialValues = new ContentValues();
//        initialValues.put(USERNAME, user.getUsername());
//        initialValues.put(AVATAR, user.getAvatar());
//        initialValues.put(WINS, user.getWins());
//        initialValues.put(LOOSES, user.getLooses());
//        initialValues.put(TIES, user.getTies());
//        // Insert it into the database.
//        return db.insert(USER_TABLE, null, initialValues);
//    }
//
//    /**
//     * Deletes an user from the database
//     * @param username user name to delete
//     * @return true iff success
//     */
//    public boolean deleteUser(String username) {
//        String where = USERNAME + "=" + username;
//        return db.delete(USER_TABLE, where, null) != 0;
//    }
//
//    // Return all data in the database.
//    public List<User> getTopTen() {
//        String where = null;
//        Cursor c = db.query(true, USER_TABLE, ALL_USER_COLUMNS,
//                where, null, null, null, WINS + " DESC", "10");
//
//        List<User> result = new ArrayList<>();
//        if (c.moveToFirst()) {
//            do {
//                String username = c.getString(c.getColumnIndex(USERNAME));
//                byte[] avatar = c.getBlob(c.getColumnIndex(AVATAR));
//                int wins = c.getInt(c.getColumnIndex(WINS));
//                int looses = c.getInt(c.getColumnIndex(LOOSES));
//                int ties = c.getInt(c.getColumnIndex(TIES));
//                result.add(new User(username, avatar, wins, looses, ties));
//            } while(c.moveToNext());
//        }
//        return result;
//    }
//
//    // Get a specific row (by rowId)
//    public User getUser(String username) {
//        String where = USERNAME + "=" + username;
//        Cursor c = 	db.query(true, USER_TABLE, ALL_USER_COLUMNS,
//                where, null, null, null, null, null);
//        if (c != null) {
//            c.moveToFirst();
//            byte[] avatar = c.getBlob(c.getColumnIndex(AVATAR));
//            int wins = c.getInt(c.getColumnIndex(WINS));
//            int looses = c.getInt(c.getColumnIndex(LOOSES));
//            int ties = c.getInt(c.getColumnIndex(TIES));
//            return new User(username, avatar, wins, looses, ties);
//        }
//        return null;
//    }
//
//    public void togleWins(String username) {
//        String sql = getIncSQL(username, WINS);
//        db.execSQL(sql);
//    }
//
//    public void togleLooses(String username) {
//        String sql = getIncSQL(username, LOOSES);
//        db.execSQL(sql);
//    }
//
//    public void togleTies(String username) {
//        String sql = getIncSQL(username, TIES);
//        db.execSQL(sql);
//    }
//
//    private String getIncSQL(String username, String incCol) {
//        return "UPDATE " + USER_TABLE + " SET " + incCol + " = " + incCol + " + 1 " +
//                "WHERE " + USERNAME + " = '" + username + "'";
//    }
//
//    private static class DatabaseHelper extends SQLiteOpenHelper
//    {
//        DatabaseHelper(Context context) {
//            super(context, DATABASE_NAME, null, DATABASE_VERSION);
//        }
//
//        @Override
//        public void onCreate(SQLiteDatabase _db) {
//            _db.execSQL(CREATE_USER_SQL);
//        }
//
//        @Override
//        public void onUpgrade(SQLiteDatabase _db, int oldVersion, int newVersion) {
//            // Destroy old database:
//            _db.execSQL("DROP TABLE IF EXISTS " + USER_TABLE);
//            // Recreate new database:
//            onCreate(_db);
//        }
//    }
}

