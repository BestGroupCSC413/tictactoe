package com.example.mac.tictactoe;

/**
 * Created by elinasuslova on 12/2/17.
 */

public class Players {
  private String player;
  private String pushId;

  public Players(){

  }
  public Players(String palyer, String pushId) {
    this.player = palyer;
    this.pushId = pushId;
  }

    public String getPlayer(){
      return player;
  }
    public void setPlayer(String palyer){
      this.player = player;
    }
    public String getPushId(){
      return pushId;
    }
    public void setPushId(String pushId){
      this.pushId = pushId;
    }


}
