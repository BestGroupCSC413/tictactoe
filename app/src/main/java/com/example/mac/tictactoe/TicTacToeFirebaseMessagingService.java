package com.example.mac.tictactoe;

/**
 * Created by elinasuslova on 12/5/17.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.RemoteInput;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.example.mac.tictactoe.HomeActivity;
import com.example.mac.tictactoe.GameActivity;
import com.example.mac.tictactoe.MainActivity;
import com.example.mac.tictactoe.LeaderBoardActivity;
import com.example.mac.tictactoe.SignInActivity;


import static com.example.mac.tictactoe.GameUtil.getCurrentUserId;
import static android.support.v4.app.NotificationCompat.PRIORITY_HIGH;
import static android.support.v4.app.NotificationCompat.PRIORITY_MAX;


public class TicTacToeFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
      super.onMessageReceived(remoteMessage);

      Log.d(TAG, "From: " + remoteMessage.getFrom());
      Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
      //createNotification(remoteMessage.getNotification().getBody());

      String fromPushId = remoteMessage.getData().get("fromPushId");
      String fromName = remoteMessage.getData().get("fromName");
      String fromId = remoteMessage.getData().get("fromId");
      String type = remoteMessage.getData().get("type");
      Log.d(TAG, "onMessageReceived");

      if(type.equals("invite")){
        Intent rejectIntent = new Intent("reject")
            .putExtra("withId", fromId)
            .putExtra("to", fromPushId);

        PendingIntent  pendingIntentReject  =
            PendingIntent.getBroadcast(this,0, rejectIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            String gameId = fromId + " - " + getCurrentUserId();

        Intent rejectAccept = new Intent("accept")
            .putExtra("withId", fromId)
            .putExtra("to", fromPushId);

        PendingIntent pendingIntentAccept =
            PendingIntent.getBroadcast(this,2, rejectAccept, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
            .setPriority(PRIORITY_MAX)
            .setContentTitle(String.format("%s invites you to play!", fromName))
            .addAction(R.drawable.accept, "Accept", pendingIntentAccept)
            .setVibrate(new long[3000])
            .addAction(R.drawable.cancel,"Reject", pendingIntentReject);

        Intent resultIntent  = new Intent(this, GameActivity.class)
            .putExtra("type", "wifi")
            .putExtra("withId", fromId)
            .putExtra("to", fromPushId);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(LeaderBoardActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
            PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(1, mBuilder.build());
      }
      else if(type.equals("accept")){
        startActivity(new Intent(getBaseContext(),GameActivity.class)
            .putExtra("type", "wifi")
            .putExtra("me", "x")
            .putExtra("gameId", getCurrentUserId()+ "-" + fromId)
            .putExtra("withId", fromId));
      }
      else if (type.equals("reject")){
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
            .setPriority(PRIORITY_MAX)
            .setContentTitle(String.format("%s rejected your invite!", fromName));

        Intent resultIntent = new Intent(this, GameActivity.class)
            .putExtra("type", "wifi");
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(LeaderBoardActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(1,mBuilder.build());
      }


    }
/*
  private void createNotification( String messageBody) {
    Intent intent = new Intent( this , GameActivity. class );
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    PendingIntent resultIntent = PendingIntent.getActivity( this , 0, intent,
        PendingIntent.FLAG_ONE_SHOT);

    Uri notificationSoundURI = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder( this)
        .setSmallIcon(R.mipmap.ic_launcher)
        .setContentTitle("Android Tutorial Point FCM Tutorial")
        .setContentText(messageBody)
        .setAutoCancel( true )
        .setSound(notificationSoundURI)
        .setContentIntent(resultIntent);

    NotificationManager notificationManager =
        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

    notificationManager.notify(0, mNotificationBuilder.build());
  }
  */
  }


