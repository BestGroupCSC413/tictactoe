package com.example.mac.tictactoe;

/**
 * Created by elinasuslova on 12/5/17.
 */
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class GameUtil {
  //private static final String LOG_TAG = GameUtil.class.getSimpleName();
  //private static DatabaseReference sFirebaseDatabaseReference =
     // FirebaseDatabase.getInstance().getReference();

  public static void savePushToken(String refreshedToken, String userId) {
    FirebaseDatabase.getInstance().getReference().child("users")
        .child(userId)
        .child("pushId")
        .setValue(refreshedToken);
  }

  public static String getCurrentUserId() {
    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    if (currentUser == null || currentUser.isAnonymous()) {
      return "";
    } else {
      return currentUser.getUid();
    }
  }

}
