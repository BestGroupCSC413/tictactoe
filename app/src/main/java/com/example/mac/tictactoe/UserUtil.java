package com.example.mac.tictactoe;


import android.app.Activity;
import android.graphics.BitmapFactory;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class UserUtil {
    private static final String LOG_TAG = UserUtil.class.getSimpleName();
    private static DatabaseReference sFirebaseDatabaseReference =
            FirebaseDatabase.getInstance().getReference();
    private static MessageLoadListener sAdapterListener;
    private static FirebaseAuth sFirebaseAuth;
    public interface MessageLoadListener {  };

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        public TextView usernameTextView;
        public TextView uidTextView;
        public ImageView userPic;
        public ImageButton add_button;
        public UserViewHolder(View v) {
            super(v);
            usernameTextView = itemView.findViewById(R.id.usernameTextView);
            uidTextView = itemView.findViewById(R.id.uidTextView);
            userPic = itemView.findViewById(R.id.player_icon);
            add_button = itemView.findViewById(R.id.button_add_game);
        }
    }

    public static FirebaseRecyclerAdapter getFirebaseAdapter(final Activity activity,
                                                             final LinearLayoutManager linearManager,
                                                             final RecyclerView recyclerView) {
//        sAdapterListener = listener;
        final FirebaseRecyclerAdapter adapter = new FirebaseRecyclerAdapter<User,
                UserViewHolder>(
                User.class,
                R.layout.player_li,
                UserViewHolder.class,
                sFirebaseDatabaseReference.child(CHILD.USERS)) {
            @Override
            protected void populateViewHolder(final UserViewHolder viewHolder,
                                              User user, int position) {
                viewHolder.usernameTextView.setText(user.getUsername());
                viewHolder.uidTextView.setText(user.getUID());
                final String uid = user.getUID();
                byte[] avatar = user.getAvatar();
                if (avatar != null) {
                    viewHolder.userPic.setImageBitmap(BitmapFactory.decodeByteArray(avatar, 0, avatar.length));

                }
                viewHolder.add_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((FindGameActivity) activity).onClickPlayer(uid);
                    }
                });
            }

//            @Override
//            public void onClick(View view) {
//
//            }

        };

        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
//                int userCount = adapter.getItemCount();
//                int lastVisiblePosition = linearManager.findLastCompletelyVisibleItemPosition();
//                recyclerView.scrollToPosition(positionStart);
            }
        });
        return adapter;
    }
}
