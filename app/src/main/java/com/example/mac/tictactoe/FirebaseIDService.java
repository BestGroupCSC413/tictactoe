package com.example.mac.tictactoe;

/**
 * Created by elinasuslova on 12/5/17.
 */
import static com.example.mac.tictactoe.GameUtil.savePushToken;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
public class FirebaseIDService extends FirebaseInstanceIdService {
  private static final String TAG = "FirebaseIDService";

  @Override
  public void onTokenRefresh() {
    // Get updated InstanceID token.
    String refreshedToken = FirebaseInstanceId.getInstance().getToken();
    Log.d(TAG, "Refreshed token: " + refreshedToken);
    FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    if (currentUser == null){
      return;

    }
    savePushToken(refreshedToken, currentUser.getUid());
  }


}


