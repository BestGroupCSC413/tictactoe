package com.example.mac.tictactoe;

/**
 * Created by elinasuslova on 12/7/17.
 */
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.example.mac.tictactoe.GameActivity;
import com.example.mac.tictactoe.Players;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.example.mac.tictactoe.GameUtil.getCurrentUserId;
public class Receiver extends BroadcastReceiver{

  private static final String TAG  = "MyReceiver";

  @Override
  public void onReceive(final Context context, final Intent intent){
    Log.d(TAG, "onReceive: "+ intent.getAction());
    FirebaseDatabase.getInstance().getReference().child("users")
        .child(getCurrentUserId())
        .addListenerForSingleValueEvent(new ValueEventListener(){

          @Override
          public void onDataChange(DataSnapshot dataSnapshot){
            Players me = dataSnapshot.getValue(Players.class);

            OkHttpClient client = new OkHttpClient();

            String to  = intent.getExtras().getString("to");
            String withId = intent.getExtras().getString("withId");
            String format = String.format("https://us-central1-tic-tac-toe-b1f6e.cloudfunctions.net/sendNotification?to=%s&fromPushId=%s&fromId=%s&fromName=%s&type=%s" , to, me.getPushId(), getCurrentUserId(), me.getPlayer(), intent.getAction());
            Log.d(TAG, "onDateChange: "+format);
            Request request = new Request.Builder().url(format).build();

            client.newCall(request).enqueue(new Callback() {
              @Override
              public void onFailure(Call call, IOException e) {

              }

              @Override
              public void onResponse(Call call, Response response) throws IOException {

              }
            });
            if (intent.getAction().equals("accept")){
              String gameId = withId +  "-" + getCurrentUserId();
              FirebaseDatabase.getInstance().getReference().child("games")
                  .child(gameId).setValue(null);

              context.startActivity(new Intent(context,GameActivity.class)
                  .putExtra("me","O")
                  .putExtra("gameId", gameId)
                  .putExtra("with", withId)
              );
            }

          }
          @Override
          public void onCancelled(DatabaseError databaseError){

          }
        });
  }

}
