package com.example.mac.tictactoe;

import android.app.Activity;
import android.graphics.BitmapFactory;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;

/**
 * Created by Sean Sutherland on 12/6/2017.
 */

public class GameRef {
    /* reference to gamedata stored per user */
    private final String gameid;
    private final String otherPlayer;
    private final long timestamp;
    /*static members*/
    private static DatabaseReference sFirebaseDatabaseReference =
            FirebaseDatabase.getInstance().getReference();
    private static FirebaseAuth sFirebaseAuth;
    public interface MessageLoadListener {  };

    public String getGameid() {
        return gameid;
    }

    public String getOtherPlayer() {
        return otherPlayer;
    }

    public long getTimestamp() {
        return timestamp;
    }


    GameRef(String uid, String otherPlayer) {
        this.gameid = uid;
        this.otherPlayer = otherPlayer;
        this.timestamp = new Date().getTime();
    }

    GameRef() {
        this("no_id", "no_id");
    }

    //// Static methods ////

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        public TextView usernameTextView;
        public TextView uidTextView;
        public ImageView userPic;
        public ImageButton add_button;

        public UserViewHolder(View v) {
            super(v);
            usernameTextView = itemView.findViewById(R.id.usernameTextView);
            uidTextView = itemView.findViewById(R.id.uidTextView);
            userPic = itemView.findViewById(R.id.player_icon);
            add_button = itemView.findViewById(R.id.button_add_game);
        }
    }

    public static FirebaseRecyclerAdapter getFirebaseAdapter(final Activity activity,
                                                             final LinearLayoutManager linearManager,
                                                             final RecyclerView recyclerView, String userID) {
//        sAdapterListener = listener;
        final FirebaseRecyclerAdapter adapter = new FirebaseRecyclerAdapter<GameRef,
                GameRef.UserViewHolder>(
                GameRef.class,
                R.layout.player_li,
                GameRef.UserViewHolder.class,
                sFirebaseDatabaseReference.child(CHILD.GAMES_BY_USER).child(userID)) {
            @Override
            protected void populateViewHolder(final GameRef.UserViewHolder viewHolder,
                                              GameRef gameref, int position) {
                final String gameid = gameref.getGameid();
                sFirebaseDatabaseReference.child(CHILD.USERS).child(gameref.getOtherPlayer())
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User player = dataSnapshot.getValue(User.class);
                        viewHolder.usernameTextView.setText(player.getUsername());
                        byte[] avatar = player.getAvatar();
                        if (avatar != null) {
                            viewHolder.userPic.setImageBitmap(BitmapFactory.decodeByteArray(avatar, 0, avatar.length));
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {}
                });

                viewHolder.uidTextView.setText(gameref.getGameid());
                viewHolder.add_button.setImageDrawable(ContextCompat.getDrawable(activity,R.drawable.ic_play_light));
                viewHolder.add_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        ((FindGameActivity) activity).onClickGame(gameid);
                    }
                });
            }
        };

        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                //super.onItemRangeInserted(positionStart, itemCount);
                int userCount = adapter.getItemCount();
                int lastVisiblePosition = linearManager.findLastCompletelyVisibleItemPosition();
                recyclerView.scrollToPosition(positionStart);
            }
        });
        return adapter;
    }
}
